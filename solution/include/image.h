#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <stdbool.h>
#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image image_create(uint32_t width, uint32_t height);

void image_free_data(struct image* img);

bool image_set_pixel(struct image* img, uint32_t row, uint32_t col, struct pixel data);

struct pixel* image_get_pixel(struct image const* img, uint32_t row, uint32_t col);

#endif
