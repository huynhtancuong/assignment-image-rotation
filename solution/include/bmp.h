#ifndef _BMP_H_
#define _BMP_H_

#include "image.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>


#pragma pack(push, 1)
/// @brief 
struct bmp_header 
{
  uint16_t bfType; // signature, must be 4D42 hex ("BM")
  uint32_t bfileSize; // size of BMP file in bytes (unreliable)
  uint32_t bfReserved; // reserved, must be zero
  uint32_t bOffBits; // offset to start of image data in bytes
  uint32_t biSize; // size of BITMAPINFOHEADER structure, must be 40
  uint32_t biWidth; // image width in pixels
  uint32_t biHeight; // image height in pixels
  uint16_t biPlanes; // number of planes in the image, must be 1
  uint16_t biBitCount; // number of bits per pixel (1, 4, 8, or 24)
  uint32_t biCompression; // compression type (0=none, 1=RLE-8, 2=RLE-4)
  uint32_t biSizeImage; // size of image data in bytes (including padding)
  uint32_t biXPelsPerMeter; // horizontal resolution in pixels per meter (unreliable)
  uint32_t biYPelsPerMeter; // vertical resolution in pixels per meter (unreliable)
  uint32_t biClrUsed; // number of colors in image, or zero
  uint32_t biClrImportant; // number of important colors, or zero
};
#pragma pack(pop)

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_HEADER,
  READ_INVALID_SIZE, // width and height must be greater than 0
  READ_INVALID_RESERVED, // reserved bit must be 0
  READ_INVALID_BITMAPINFOHEADER_SIZE, // biSize must be 40
  READ_INVALID_PLANES, // biPlanes must be 1
  READ_INVALID_BITS_PER_PIXEL, // bits per pixel must be 24
  IMAGE_CREATE_FAIL,
  READ_INVALID_DATA
  };


/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_DATA_ERROR,
  WRITE_HEADER_ERROR,
  IMAGE_INVALID
};


enum read_status from_bmp( FILE* in, struct image* img );

enum write_status to_bmp( FILE* out, struct image const* img );

struct bmp_header bmp_header_create(uint32_t width, uint32_t height); // create bmp_header from width and height
struct bmp_header bmp_header_create_offbits(uint32_t width, uint32_t height, uint32_t bOffBits); // create bmp_header with modified offset bits

#endif

