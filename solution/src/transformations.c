#include "../include/transformations.h"
#include <stdio.h>

struct image rotate( struct image const* source )
{

    struct image out = image_create(source->height, source->width);

    for (size_t row = 0; row < source->height; row++)
    {
        for (size_t col = 0; col < source->width; col++)
        {
            // out.data[col*source->height + (source->height - 1 - row )] =
            //     source->data[row*source->width + col];
            struct pixel data = *image_get_pixel(source, row, col);
            image_set_pixel(&out, col, source->height - 1 - row, data);
        }
    }
    return out;
}

