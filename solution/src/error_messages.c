#include "error_messages.h"
#include "bmp.h"
#include "file_io.h"


static const char* const bmp_read_status_string[] = {
    [READ_OK] = "[Info] Read OK.\n",
    [READ_INVALID_SIGNATURE] = "[Error] Invalid header signature. Signature must be 'BM'.\n",
    [READ_INVALID_HEADER] = "[Error] Can not read header section.\n",
    [READ_INVALID_SIZE] = "[Error] Width and height must be greater than 0.\n",
    [READ_INVALID_RESERVED] = "[Error] Reserved bits must be 0.\n",
    [READ_INVALID_BITMAPINFOHEADER_SIZE] = "[Error] Invalid size of BITMAPINFOHEADER. Must be 40.\n",
    [READ_INVALID_PLANES] = "[Error] Invalid number of planes. Must be 1.\n",
    [READ_INVALID_BITS_PER_PIXEL] = "[Error] Invalid number of bits per pixel.\n",
    [IMAGE_CREATE_FAIL] = "[Error] Could not create image object. Maybe the heap is full.\n",
    [READ_INVALID_DATA] = "[Error] Could not read the data section. Maybe the data section was damaged.\n"
};

static const char* const bmp_write_status_string[] = {
    [WRITE_OK] = "[Info] Write OK.\n",
    [WRITE_DATA_ERROR] = "[Error] Could not write image data.\n",
    [WRITE_HEADER_ERROR] = "[Error] Could not write image header.\n", 
    [IMAGE_INVALID] = "[Error] Image is invalid."
};

static const char* const file_io_status_string[] = {
    [OPEN_OK] = "[Info] Open OK.\n",
    [OPEN_FAIL] = "[Error] Failed to open file.\n",
    [CLOSE_OK] = "[Info] Close OK.\n",
    [CLOSE_FAIL] = "[Error] Failed to close file.\n"
};

const char* bmp_read_get_error_message(size_t index) {
    return bmp_read_status_string[index];
}

const char* bmp_write_get_error_message(size_t index) {
    return bmp_write_status_string[index];
}

const char* file_io_get_error_message(size_t index) {
    return file_io_status_string[index];
}


