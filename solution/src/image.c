#include "../include/image.h"
#include <stdbool.h>
#include <stdlib.h>

/// @brief This function create a instance of struct image. Allocate memory in the heap with size, which calculated from width and height.
/// @param width width of the image
/// @param height height of the image
struct image image_create(uint32_t width, uint32_t height) 
{
    struct image img = { .width = 0, .height = 0, .data = (struct pixel*) NULL }; // create img instance 
    // allocate memory for data array of img
    if (width != 0 && height != 0) img.data = malloc( sizeof(struct pixel) * (width * height) );
    if (img.data != NULL) // if allocation is ok
    {
        img.width = width;
        img.height = height;
    }

    return img;
}

/// @brief A function that deallocate the data field for instance of struct image. And also set the width and height to 0.
/// @param *img address of struct image instance
void image_free_data(struct image* img) 
{
    // free data field of img
    free(img->data);
    // set width and height of img to 0
    img-> width = 0;
    img-> height = 0; 
}

bool image_set_pixel(struct image* img, uint32_t row, uint32_t col, struct pixel data)
{
    if ((row <= img->height) && (col <= img->width)) {
        img->data[row*(img->width) + col] = data;
        return true;
    }
    return false;
}

struct pixel* image_get_pixel(struct image const* img, uint32_t row, uint32_t col)
{
    if ((row <= img->height) && (col <= img->width)) {
        return &img->data[row*(img->width) + col];
    }
    return (struct pixel*)NULL;
}

