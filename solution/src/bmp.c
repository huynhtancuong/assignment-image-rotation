#include "bmp.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>

#define BMP_HEADER_SIGNATURE  0x4D42
#define BMP_HEADER_RESERVED   0
#define BMP_HEADER_OFFSET     54
#define BMP_HEADER_SIZE_OF_BITMAPINFOHEADER 40
#define BMP_HEADER_NUMBER_OF_PLANES   1
#define BMP_HEADER_NUMBER_OF_BITS_PER_PIXEL 24
#define BMP_HEADER_COMPRESSION_TYPE 0 // 0 = NONE, 1= RLE-8, 2=RLE-4
#define BMP_HEADER_NUMBER_OF_COLOR 0
#define BMP_HEADER_NUMBER_OF_IMPORTANT_COLOR 0
#define BMP_HEADER_HORIZONTAL_PPM 2834
#define BMP_HEADER_VERTICAL_PPM 2834



static bool read_header( FILE* f, struct bmp_header* header ) {
    fseek(f, 0, SEEK_SET);
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

static bool write_header( FILE* f, struct bmp_header* header) 
{
    // fseek(f, 0, SEEK_SET); // fseek to zero does not make sense for stdout
    return fwrite(header, sizeof(struct bmp_header), 1, f);
}

static uint8_t get_padding_from_width(uint32_t width) {
    return 4 - (width*sizeof(struct pixel) % 4);
}

/// @brief A function which create bmp_header from given width and height.
struct bmp_header bmp_header_create(const uint32_t width, const uint32_t height)
{
    // calculate needed params
    uint8_t padding_size =  get_padding_from_width(width);
    uint32_t bfileSize = sizeof(struct bmp_header) + (height + width) * sizeof(struct pixel) + height * padding_size; // size of file = size of header + size of data + size of paddings
    uint32_t biSizeImage = (height + width) * sizeof(struct pixel) + height * padding_size; // size of image data (include padding, not include header)
    // create header 
    struct bmp_header header = {
        .bfType         = BMP_HEADER_SIGNATURE,
        .bfileSize      = bfileSize,
        .bfReserved     = BMP_HEADER_RESERVED,
        .bOffBits       = BMP_HEADER_OFFSET,
        .biSize         = BMP_HEADER_SIZE_OF_BITMAPINFOHEADER,
        .biWidth        = width,
        .biHeight       = height,
        .biPlanes       = BMP_HEADER_NUMBER_OF_PLANES,
        .biBitCount     = BMP_HEADER_NUMBER_OF_BITS_PER_PIXEL,
        .biCompression  = BMP_HEADER_COMPRESSION_TYPE,
        .biSizeImage    = biSizeImage,
        .biXPelsPerMeter    = BMP_HEADER_HORIZONTAL_PPM,
        .biYPelsPerMeter    = BMP_HEADER_VERTICAL_PPM,
        .biClrUsed      = BMP_HEADER_NUMBER_OF_COLOR,
        .biClrImportant = BMP_HEADER_NUMBER_OF_IMPORTANT_COLOR
    };
    // return the result
    return header;
}

/// @brief A wapper function around bmp_header_create.
struct bmp_header bmp_header_create_offbits(const uint32_t width, const uint32_t height, const uint32_t bOffBits)
{
    struct bmp_header header = bmp_header_create(width, height);
    header.bOffBits = bOffBits;
    
    return header;
}

static bool bmp_header_is_sign_valid(struct bmp_header header)
{
    if (header.bfType != BMP_HEADER_SIGNATURE) 
        return false;
    return true;
}
static bool bmp_header_is_size_valid(struct bmp_header header)
{
    if (header.biWidth > 0 && header.biHeight >0) 
        return true;
    return false;
}
static bool bmp_header_is_reserved_valid(struct bmp_header header)
{
    if (header.bfReserved == BMP_HEADER_RESERVED) 
        return true;
    return false;
}
static bool bmp_header_is_bitmapinfoheader_size_valid(struct bmp_header header)
{
    if (header.biSize == BMP_HEADER_SIZE_OF_BITMAPINFOHEADER) 
        return true;
    return false;
}
static bool bmp_header_is_planes_valid(struct bmp_header header)
{
    if (header.biPlanes == BMP_HEADER_NUMBER_OF_PLANES) 
        return true;
    return false;
}
static bool bmp_header_is_BPP_valid(struct bmp_header header)
{
    if (header.biBitCount == BMP_HEADER_NUMBER_OF_BITS_PER_PIXEL) 
        return true;
    return false;
}

static enum read_status bmp_header_checker(struct bmp_header header)
{
    if (!bmp_header_is_sign_valid(header))
        return READ_INVALID_SIGNATURE;
    if (!bmp_header_is_size_valid(header))
        return READ_INVALID_SIZE; 
    if (!bmp_header_is_reserved_valid(header))
        return READ_INVALID_RESERVED;
    if (!bmp_header_is_bitmapinfoheader_size_valid(header))
        return READ_INVALID_BITMAPINFOHEADER_SIZE;
    if (!bmp_header_is_planes_valid(header))
        return READ_INVALID_PLANES;
    if (!bmp_header_is_BPP_valid(header))
        return READ_INVALID_BITS_PER_PIXEL;
    return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* img )
{
    /**
     *  Read header section from input file
     */

    // create instance h of bmp_header
    struct bmp_header header = bmp_header_create(0, 0);
    // read header of the source image
    if (!read_header(in, &header)) 
        return READ_INVALID_HEADER;
    // check if header is valid
    enum read_status header_check_status = bmp_header_checker(header);
    if (header_check_status != READ_OK) {
        return header_check_status;
    }
    
    
    /**
     * Read data section from input file
     */
    struct image local_img = image_create(header.biWidth, header.biHeight);
    if (local_img.width == 0 || local_img.height == 0)
        return IMAGE_CREATE_FAIL;

    uint8_t padding_size = get_padding_from_width(local_img.width);

    for (size_t row = 0; row < local_img.height; row++)
    {
        if ( fread( &local_img.data[row * (local_img.width)], sizeof(struct pixel), local_img.width, in ) != local_img.width)
        {
            image_free_data(&local_img);
            return READ_INVALID_DATA;
        }

        if ( fseek(in, padding_size, SEEK_CUR) != 0 ) {
            image_free_data(&local_img);
            return READ_INVALID_DATA;
        }
    }

    *img = local_img; // transactionality

    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img )
{
    // check if image is valid
    if (img->height == 0 || img->width == 0 || img->data == NULL)
        return IMAGE_INVALID;

    // write header
    struct bmp_header header = bmp_header_create(img->width, img->height);
    if ( !write_header(out, &header) )
        return WRITE_HEADER_ERROR;

    // write image to file (with offset)
    uint8_t padding_size =  get_padding_from_width(img->width);
    fseek(out, header.bOffBits, SEEK_SET);
    
    for (size_t row = 0; row < (img->height); row++) 
    {
        if ( !fwrite( &img->data[row * (img->width)], sizeof(struct pixel), img->width, out ) )
        {
            return WRITE_DATA_ERROR;
        }
        if (padding_size != 0) fseek(out, padding_size, SEEK_CUR);
    }


    return WRITE_OK;
}

