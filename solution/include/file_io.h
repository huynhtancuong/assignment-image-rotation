#ifndef _FILEIO_H_
#define _FILEIO_H_

#include <stdbool.h>
#include <stdio.h>

enum open_mode {
    READ_BINARY,
    WRITE_BINARY
};

enum file_io_status {
    OPEN_OK,
    OPEN_FAIL,
    CLOSE_OK,
    CLOSE_FAIL
};

bool file_open(FILE** fptr, char const* file_name, enum open_mode mode);
bool file_close(FILE** fptr);


#endif

