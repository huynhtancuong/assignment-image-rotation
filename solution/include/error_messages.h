#ifndef _ERROR_MESS_H_
#define _ERROR_MESS_H_

#include <stddef.h>

const char* bmp_read_get_error_message(size_t index);

const char* bmp_write_get_error_message(size_t index);

const char* file_io_get_error_message(size_t index);

#endif

