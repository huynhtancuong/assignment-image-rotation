#include "bmp.h"
#include "error_messages.h"
#include "file_io.h"
#include "image.h"
#include "transformations.h"
#include "util.h"
#include <stdio.h>

#define FILE_OPEN_IN_ERR    "[Error] Failed to open input file.\n"
#define FILE_OPEN_OUT_ERR   "[Error] Failed to open output file.\n"
#define FILE_CLOSE_IN_ERR   "[Error] Failed to close input file.\n"
#define FILE_CLOSE_OUT_ERR  "[Error] Failed to close output file.\n"

void usage(void);
void usage(void) {
    fprintf(stderr, "Usage: ./image-transformer <source-image> <transformed-image>\n");
}


int main( int argc, char** argv ) {
    if (argc != 3) usage();
    if (argc < 3) err("Not enough arguments \n");
    if (argc > 3) err("Too many arguments \n");

    // open the input file to read
    FILE* fptr_input = NULL;
    if (!file_open(&fptr_input, argv[1], READ_BINARY)) // check if file open is ok
        err(FILE_OPEN_IN_ERR);
    
    // open the ouput file to write
    FILE* fptr_out = NULL;
    if (!file_open(&fptr_out, argv[2], WRITE_BINARY)) // check if file open is ok
        err(FILE_OPEN_OUT_ERR);
    

    /**
     * Read image from the input file
     */

    // create instance img of image
    struct image source_img = image_create(0, 0);
    // read source-image data to img
    enum read_status read_bmp_status = from_bmp(fptr_input, &source_img);
    if (read_bmp_status != READ_OK) // check if read status is ok
        err(bmp_read_get_error_message(read_bmp_status));
    
    // perform transformation on img
    struct image rotated_img = rotate(&source_img);
    // write transformed img to <transformed-image>
    enum write_status write_bmp_status = to_bmp(fptr_out, &rotated_img);
    if (write_bmp_status != WRITE_OK) // check if write status is ok
        err(bmp_write_get_error_message(write_bmp_status));

    // free the heap
    image_free_data(&source_img);
    image_free_data(&rotated_img);
    
    // close input and output files
    if (!file_close(&fptr_input)) // check if input file closed successfully
        err(FILE_CLOSE_IN_ERR);

    if (!file_close(&fptr_out)) // check if output file closed successfully
        err(FILE_CLOSE_OUT_ERR); 

    return 0;
}
   
