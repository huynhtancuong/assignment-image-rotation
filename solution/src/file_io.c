#include "../include/file_io.h"

bool file_open(FILE** fptr, char const* file_name, enum open_mode mode)
{
    switch (mode)
    {
    case READ_BINARY:
        *fptr = fopen(file_name, "rb");
        break;
    case WRITE_BINARY:
        *fptr = fopen(file_name, "wb");
        break;
    default:
        break;
    }
    
    if (*fptr) // if file open is ok
    {
        return true;
    }

    return false; // if file open is not ok
}


bool file_close(FILE** fptr)
{
    if (fclose(*fptr) == 0) // check if file closed ok
        return true;
    else
        return false; // file closed is not ok
}

